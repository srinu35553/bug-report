package com.google.developer.bugmaster;

import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v4.util.Pair;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.google.developer.bugmaster.data.Insect;
import com.google.developer.bugmaster.data.InsectLoader;
import com.google.developer.bugmaster.data.InsectRecyclerAdapter;
import com.google.developer.bugmaster.data.Utils;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements
        View.OnClickListener, LoaderManager.LoaderCallbacks<List<Insect>>, InsectRecyclerAdapter.InsectClickListener {

    private static final String TAG = MainActivity.class.getSimpleName();

    public static final String INSECT = "insect";
    private static final int MAIN_LOADER_ID = 1;

    private InsectRecyclerAdapter mInsectsAdapter;

    private boolean mSortByDanger;
    public List<Insect> mInsectsList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(this);

        RecyclerView insectsRv = (RecyclerView) findViewById(R.id.recycler_view);
        insectsRv.setLayoutManager(new LinearLayoutManager(this));
        mInsectsAdapter = new InsectRecyclerAdapter(mInsectsList, this);
        insectsRv.setAdapter(mInsectsAdapter);

        // Get the sort preference on configuration change
        if(savedInstanceState != null) {
            mSortByDanger = savedInstanceState.getBoolean(InsectLoader.SORT_BY_DANGER);
        }
        Bundle args = new Bundle();
        args.putBoolean(InsectLoader.SORT_BY_DANGER, mSortByDanger);
        getSupportLoaderManager().initLoader(MAIN_LOADER_ID, args, this);
    }

    /* Save the sort preference on configuration change */
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putBoolean(InsectLoader.SORT_BY_DANGER, mSortByDanger);
        super.onSaveInstanceState(outState);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_sort:
                mSortByDanger = !mSortByDanger;
                Bundle args = new Bundle();
                args.putBoolean(InsectLoader.SORT_BY_DANGER, mSortByDanger);
                getSupportLoaderManager().restartLoader(MAIN_LOADER_ID, args, this);
                return true;
            case R.id.action_settings:
                Intent intent = new Intent(this, SettingsActivity.class);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /* Click events in Floating Action Button */
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fab:
                // Get the question and answer set
                Pair randomInsects = Utils.pickRandomInsects(mInsectsList, QuizActivity.ANSWER_COUNT);
                ArrayList<Insect> quizInsects = (ArrayList<Insect>) randomInsects.first;
                // Start the quiz activity
                Intent quizIntent = new Intent(this, QuizActivity.class);
                quizIntent.putExtra(QuizActivity.EXTRA_INSECTS, quizInsects);
                quizIntent.putExtra(QuizActivity.EXTRA_ANSWER,
                        quizInsects.get((Integer) randomInsects.second));
                startActivity(quizIntent);
                break;
            default:
                Log.d(TAG, "No functionality implemented for the click on view " + v.getId());
                break;
        }
    }

    @Override
    public Loader<List<Insect>> onCreateLoader(int id, Bundle args) {
        return new InsectLoader(MainActivity.this, args);
    }

    @Override
    public void onLoadFinished(Loader<List<Insect>> loader, List<Insect> data) {
        this.mInsectsList = data;
        mInsectsAdapter.updateInsectsList(mInsectsList);
    }

    @Override
    public void onLoaderReset(Loader<List<Insect>> loader) {

    }

    /**
     * OnClick listener for the insect item on main screen
     * @param insect
     */
    @Override
    public void onClick(Insect insect) {
        // Start the details activity
        Intent detailIntent = new Intent(this, InsectDetailsActivity.class);
        detailIntent.putExtra(INSECT, insect);
        startActivity(detailIntent);
    }
}
