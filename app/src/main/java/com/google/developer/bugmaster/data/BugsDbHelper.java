package com.google.developer.bugmaster.data;

import android.content.ContentValues;
import android.content.Context;
import android.content.res.Resources;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.google.developer.bugmaster.R;
import com.google.developer.bugmaster.data.DatabaseManager.InsectDbEntry;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Database helper class to facilitate creating and updating
 * the database from the chosen schema.
 */
public class BugsDbHelper extends SQLiteOpenHelper {
    private static final String TAG = BugsDbHelper.class.getSimpleName();

    private static final String DATABASE_NAME = "insects.db";
    private static final int DATABASE_VERSION = 1;

    // Keys for parsing the raw json data of the insects
    private static final String INSECTS = "insects";
    private static final String FRIENDLY_NAME = "friendlyName";
    private static final String SCIENTIFIC_NAME = "scientificName";
    private static final String CLASSIFICATION = "classification";
    private static final String IMAGE_ASSET = "imageAsset";
    private static final String DANGER_LEVEL = "dangerLevel";


    //Used to read data from res/ and assets/
    private Resources mResources;

    public BugsDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);

        mResources = context.getResources();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        final String SQL_CREATE_INSECTS_TABLE = "CREATE TABLE " + InsectDbEntry.TABLE_NAME + " (" +
                InsectDbEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +

                InsectDbEntry.COLUMN_FRIENDLY_NAME + " TEXT NOT NULL, " +
                InsectDbEntry.COLUMN_SCIENTIFIC_NAME + " TEXT NOT NULL, " +
                InsectDbEntry.COLUMN_CLASSIFICATION + " TEXT NOT NULL, " +
                InsectDbEntry.COLUMN_IMAGE_ASSET + " TEXT NOT NULL, " +
                InsectDbEntry.COLUMN_DANGER_LEVEL + " INTEGER NOT NULL, " +
                // Replace strategy to ensure the application have just one entry per insect
                " UNIQUE (" + InsectDbEntry.COLUMN_FRIENDLY_NAME + ", " +
                InsectDbEntry.COLUMN_SCIENTIFIC_NAME + ") ON CONFLICT REPLACE);";

        try {
            // Create the bug table using the SQL CREATE query from above
            db.execSQL(SQL_CREATE_INSECTS_TABLE);
            readInsectsFromResources(db);
        } catch (SQLException e) {
            Log.e(TAG, "Error creating the INSECTS table: " + e.getLocalizedMessage());
            e.printStackTrace();
        } catch (JSONException e) {
            Log.e(TAG, "Error parsing the JSON data: " + e.getLocalizedMessage());
            e.printStackTrace();
        } catch (IOException e) {
            Log.e(TAG, "Error reading the JSON file: " + e.getLocalizedMessage());
            e.printStackTrace();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // SQLite db is upgraded when version changes (schema change). So, if version changes,
        // simply delete the current table and create a new one.
        if (newVersion != oldVersion) {
            db.execSQL("DROP TABLE IF EXISTS " + InsectDbEntry.TABLE_NAME);
            onCreate(db);
        }
    }

    /**
     * Streams the JSON data from insect.json, parses it, and inserts it into the
     * provided {@link SQLiteDatabase}.
     *
     * @param db Database where objects should be inserted.
     * @throws IOException
     * @throws JSONException
     */
    private void readInsectsFromResources(SQLiteDatabase db) throws IOException, JSONException {
        StringBuilder builder = new StringBuilder();
        InputStream in = mResources.openRawResource(R.raw.insects);
        BufferedReader reader = new BufferedReader(new InputStreamReader(in));

        String line;
        while ((line = reader.readLine()) != null) {
            builder.append(line);
        }

        //Parse resource into key/values
        final String rawJson = builder.toString();
        // Create a JSON object from the rawJson string
        JSONObject jsonObject = new JSONObject(rawJson);
        // Get the 'insects' array from the JSON object
        JSONArray insects = jsonObject.getJSONArray(INSECTS);
        // Go through each insect object and insert it into the table
        for (int i = 0; i < insects.length(); i++) {
            JSONObject insect = insects.getJSONObject(i);

            // Create content values for the parsed data
            // to be later used to insert into the table
            ContentValues insectValues = new ContentValues();
            insectValues.put(InsectDbEntry.COLUMN_FRIENDLY_NAME, insect.getString(FRIENDLY_NAME));
            insectValues.put(InsectDbEntry.COLUMN_SCIENTIFIC_NAME, insect.getString(SCIENTIFIC_NAME));
            insectValues.put(InsectDbEntry.COLUMN_CLASSIFICATION, insect.getString(CLASSIFICATION));
            insectValues.put(InsectDbEntry.COLUMN_IMAGE_ASSET, insect.getString(IMAGE_ASSET));
            insectValues.put(InsectDbEntry.COLUMN_DANGER_LEVEL, insect.getInt(DANGER_LEVEL));

            // Insert the parse data into the table
            db.insert(InsectDbEntry.TABLE_NAME, null, insectValues);
        }
    }
}
