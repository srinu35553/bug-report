package com.google.developer.bugmaster.reminders;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.support.v4.app.NotificationCompat;
import android.support.v4.util.Pair;
import android.util.Log;

import com.google.developer.bugmaster.QuizActivity;
import com.google.developer.bugmaster.R;
import com.google.developer.bugmaster.data.DatabaseManager;
import com.google.developer.bugmaster.data.Insect;
import com.google.developer.bugmaster.data.Utils;

import java.util.ArrayList;
import java.util.List;

public class ReminderService extends IntentService {

    private static final String TAG = ReminderService.class.getSimpleName();

    private static final int NOTIFICATION_ID = 42;

    public ReminderService() {
        super(TAG);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Log.d(TAG, "Quiz reminder event triggered");

        // Get the insects list from database to choose random insects
        List<Insect> insects = loadData();
        // Get the question and answer set
        Pair randomInsects = Utils.pickRandomInsects(insects, QuizActivity.ANSWER_COUNT);
        ArrayList<Insect> quizInsects = (ArrayList<Insect>) randomInsects.first;
        // Start the quiz activity
        Intent quizIntent = new Intent(this, QuizActivity.class);
        quizIntent.putExtra(QuizActivity.EXTRA_INSECTS, quizInsects);
        quizIntent.putExtra(QuizActivity.EXTRA_ANSWER,
                quizInsects.get((Integer) randomInsects.second));
        PendingIntent operation =
                PendingIntent.getActivity(this, 0, quizIntent, PendingIntent.FLAG_CANCEL_CURRENT);

        Notification note = new NotificationCompat.Builder(this)
                .setContentTitle(getString(R.string.notification_title))
                .setContentText(getString(R.string.notification_text))
                .setSmallIcon(R.drawable.ic_bug_empty)
                .setContentIntent(operation)
                .setAutoCancel(true)
                .build();

        //Present a notification to the user
        NotificationManager manager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        if (manager != null) {
            manager.notify(NOTIFICATION_ID, note);
            AlarmReceiver.scheduleAlarm(this);
        }
    }

    /*
     * Since loaders are not designed to be used in services and a loader
     * is not really necessary in a Service, let alone IntentService,
     * we can load insects from the database without using the InsectLoader.
     */
    private List<Insect> loadData() {
        DatabaseManager dataManager = DatabaseManager.getInstance(this);
        Cursor insectsCursor = dataManager.queryAllInsects(null);

        List<Insect> insects = new ArrayList<>();
        if (insectsCursor != null && insectsCursor.moveToFirst()){
            do{
                Insect insect = new Insect(insectsCursor);
                if(insect.scientificName != null) {
                    insects.add(insect);
                }
            }while(insectsCursor.moveToNext());
            insectsCursor.close();
        }

        return insects;
    }
}
