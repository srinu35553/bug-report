package com.google.developer.bugmaster.views;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.TextView;

import com.google.developer.bugmaster.R;

/**
 * Suppressing the warning to use AppCompatTextView as the requirements suggested not to modify
 * the given classes in the project. Anyways, since this is a custom view, it won't matter if it
 * is a TextView or AppCompatTextView as some of the effects such as tinting that appCompat can
 * handle are not going to be applied for custom views.
 */
@SuppressLint("AppCompatCustomView")
public class DangerLevelView extends TextView {

    private int mDangerLevel;

    public DangerLevelView(Context context) {
        super(context);
    }

    public DangerLevelView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public DangerLevelView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public DangerLevelView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    public void setDangerLevel(int dangerLevel) {
        this.mDangerLevel = dangerLevel;

        // Set the danger level text
        this.setText(Integer.toString(mDangerLevel));

        // Get the corresponding danger level color from the colors list
        String[] colors = getResources().getStringArray(R.array.dangerColors);
        int color = Color.parseColor(colors[mDangerLevel - 1]);

        // Set the color to the view's background drawable
        Drawable drawable = this.getBackground().getCurrent();
        drawable.setColorFilter(color, PorterDuff.Mode.MULTIPLY);
    }

    public int getDangerLevel() {
        if (mDangerLevel != 0) {
            return mDangerLevel;
        }
        return -1;
    }
}
