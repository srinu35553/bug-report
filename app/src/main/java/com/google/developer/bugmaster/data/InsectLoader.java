package com.google.developer.bugmaster.data;

import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.content.AsyncTaskLoader;

import java.util.ArrayList;
import java.util.List;

/**
 * Custom Loader to make the db queries in the background thread.
 * (Not using a cursor loader as we have not implemented
 * a content provider for this app)
 */
public class InsectLoader extends AsyncTaskLoader<List<Insect>> {

    // Keys for the loader arguments
    public static final String IS_BUG_DETAILS = "bug_details";
    public static final String BUG_ID = "bug_id";
    public static final String SORT_BY_DANGER = "sort_by_danger";

    // Variables for single bug query
    private boolean mIsBugDetails;
    private int mBugId;

    // Sort order for all bugs query
    private boolean mSortByDanger;

    // For caching the mInsectsList data
    private List<Insect> mInsectsList = new ArrayList<>();

    public InsectLoader(Context context, Bundle args) {
        super(context);

        if(args != null) {
            mIsBugDetails = args.getBoolean(IS_BUG_DETAILS);
            mBugId = args.getInt(BUG_ID);
            mSortByDanger = args.getBoolean(SORT_BY_DANGER);
        }
    }

    @Override
    protected void onStartLoading() {
        super.onStartLoading();
        // Deliver cached data if it exists, else force load
        if(mInsectsList.size() > 0) {
            deliverResult(mInsectsList);
        } else {
            // Force load to start an asynchronous data load
            forceLoad();
        }
    }

    /**
     * Use the {@link DatabaseManager} to get the insects cursor and parse the cursor
     * to get the list of insects
     *
     * @return {@link List<Insect>} list of insects
     */
    @Override
    public List<Insect> loadInBackground() {
        DatabaseManager dataManager = DatabaseManager.getInstance(getContext());
        Cursor insectsCursor;
        if(mIsBugDetails) {
            insectsCursor = dataManager.queryInsectsById(mBugId);
        } else {
            String sortOrder = null;
            if(mSortByDanger) {
                sortOrder = DatabaseManager.DANGER_LEVEL_SORT;
            }
            insectsCursor = dataManager.queryAllInsects(sortOrder);
        }

        if (insectsCursor != null && insectsCursor.moveToFirst()){
            do{
                Insect insect = new Insect(insectsCursor);
                if(insect.scientificName != null) {
                   mInsectsList.add(insect);
                }
            }while(insectsCursor.moveToNext());
            insectsCursor.close();
        }

        return mInsectsList;
    }

    @Override
    public void deliverResult(List<Insect> data) {
        super.deliverResult(data);
    }
}
