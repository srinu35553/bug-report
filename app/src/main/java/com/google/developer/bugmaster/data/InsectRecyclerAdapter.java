package com.google.developer.bugmaster.data;

import android.database.Cursor;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.developer.bugmaster.R;
import com.google.developer.bugmaster.views.DangerLevelView;

import java.util.List;

/**
 * RecyclerView adapter extended with project-specific required methods.
 */

public class InsectRecyclerAdapter extends
        RecyclerView.Adapter<InsectRecyclerAdapter.InsectHolder> {

    public interface InsectClickListener {
        void onClick(Insect insect);
    }

    private Cursor mCursor;
    // Will be using list of insects rather than cursor as the
    // custom loader has already processed the cursor.
    private List<Insect> mInsectsList;
    private InsectClickListener mListener;

    public InsectRecyclerAdapter(List<Insect> insectsList, InsectClickListener listener) {
        this.mInsectsList = insectsList;
        this.mListener = listener;
    }

    /* ViewHolder for each insect item */
    public class InsectHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        DangerLevelView dangerLevelView;
        TextView friendlyName, scientificName;

        InsectHolder(View itemView) {
            super(itemView);
            dangerLevelView = (DangerLevelView) itemView.findViewById(R.id.danger_level);
            friendlyName = (TextView) itemView.findViewById(R.id.friendly_name);
            scientificName = (TextView) itemView.findViewById(R.id.scientific_name);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            mListener.onClick(mInsectsList.get(getAdapterPosition()));
        }
    }

    @Override
    public InsectHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.insect_list_item, parent, false);

        return new InsectHolder(itemView);
    }

    @Override
    public void onBindViewHolder(InsectHolder holder, int position) {
        Insect insect = mInsectsList.get(position);
        holder.dangerLevelView.setDangerLevel(insect.dangerLevel);
        holder.friendlyName.setText(insect.name);
        holder.scientificName.setText(insect.scientificName);
    }

    @Override
    public int getItemCount() {
        return mInsectsList.size();
    }

    /**
     * Return the {@link Insect} represented by this item in the adapter.
     *
     * @param position Adapter item position.
     *
     * @return A new {@link Insect} filled with this position's attributes
     *
     * @throws IllegalArgumentException if position is out of the adapter's bounds.
     */
    public Insect getItem(int position) {
        if (position < 0 || position >= getItemCount()) {
            throw new IllegalArgumentException("Item position is out of adapter's range");
        } else if (mInsectsList != null && mInsectsList.size() > 0){
            // Modified this implementation to use the insects list directly rather than cursor
            // as it has been handled in InsectLoader
            return mInsectsList.get(position);
        }
        return null;
    }

    /**
     * Update the insects list when the {@link InsectLoader} is finished loading the data
     * @param insectsList
     */
    public void updateInsectsList(List<Insect> insectsList) {
        this.mInsectsList = insectsList;
        notifyDataSetChanged();
    }
}
