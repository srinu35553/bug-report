package com.google.developer.bugmaster;

import android.support.test.espresso.contrib.RecyclerViewActions;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.support.v7.widget.RecyclerView;

import com.google.developer.bugmaster.data.Insect;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.List;
import java.util.Random;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.endsWith;

@RunWith(AndroidJUnit4.class)
public class InsectDetailsActivityTest {
    @Rule
    public ActivityTestRule<MainActivity> mActivityRule =
            new ActivityTestRule<>(MainActivity.class);

    @Test
    public void ensureDetailsDisplayed() {
        // List of insects displayed on the main screen
        List<Insect> insects = mActivityRule.getActivity().mInsectsList;

        // Select a random position to open from the insects list
        int position = selectRandomItem();
        onView(withId(R.id.recycler_view)).perform(
                // Scroll to the random position selected
                RecyclerViewActions.scrollToPosition(position),
                // Click the item to open insect details
                RecyclerViewActions.actionOnItemAtPosition(position, click())
        );

        // Get the insect at the selected position
        Insect insect = insects.get(position);
        // Check if the friendly name of the insect is displayed
        onView(withId(R.id.friendly_name))
                .check(matches(withText(insect.name)));
        // Check if the scientific name is displayed
        onView(withId(R.id.scientific_name))
                .check(matches(withText(insect.scientificName)));
        // Check if classification is displayed
        onView(withId(R.id.classification))
                .check(matches(withText(endsWith(insect.classification))));
    }

    private int selectRandomItem() {
        //Get the recycler view
        RecyclerView insectsRv = (RecyclerView) mActivityRule
                .getActivity().findViewById(R.id.recycler_view);
        int length = insectsRv.getAdapter().getItemCount();

        // Generate random number within the range 0(inclusive) - length(exclusive)
        Random random = new Random();
        return random.nextInt(length);
    }
}
