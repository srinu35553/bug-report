package com.google.developer.bugmaster.data;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteQueryBuilder;
import android.provider.BaseColumns;

/**
 * Singleton that controls access to the SQLiteDatabase instance
 * for this application.
 */
public class DatabaseManager {
    private static DatabaseManager sInstance;

    public static synchronized DatabaseManager getInstance(Context context) {
        if (sInstance == null) {
            sInstance = new DatabaseManager(context.getApplicationContext());
        }

        return sInstance;
    }

    private BugsDbHelper mBugsDbHelper;

    private DatabaseManager(Context context) {
        mBugsDbHelper = new BugsDbHelper(context);
    }

    private static final SQLiteQueryBuilder bugsQueryBuilder;

    static{
        // SQLite query builder to be used to build all the db queries
        bugsQueryBuilder = new SQLiteQueryBuilder();
        // Set the table name
        bugsQueryBuilder.setTables(InsectDbEntry.TABLE_NAME);
    }

    // 'Get Insect by ID' query selection
    public static final String INSECT_BY_ID = InsectDbEntry._ID + " = ?";
    // Sort order query strings
    public static final String FRIENDLY_NAME_SORT = InsectDbEntry.COLUMN_FRIENDLY_NAME + " ASC";
    public static final String DANGER_LEVEL_SORT = InsectDbEntry.COLUMN_DANGER_LEVEL + " DESC";

    /**
     * Return a {@link Cursor} that contains every insect in the database.
     *
     * @param sortOrder Optional sort order string for the query, can be null
     * @return {@link Cursor} containing all insect results.
     */
    public Cursor queryAllInsects(String sortOrder) {
        // Setting default sort order by friendly name
        if(sortOrder == null) {
            sortOrder = FRIENDLY_NAME_SORT;
        }
        return bugsQueryBuilder.query(mBugsDbHelper.getReadableDatabase(),
                null,
                null,
                null,
                null,
                null,
                sortOrder);
    }

    /**
     * Return a {@link Cursor} that contains a single insect for the given unique id.
     *
     * @param id Unique identifier for the insect record.
     * @return {@link Cursor} containing the insect result.
     */
    public Cursor queryInsectsById(int id) {
        return bugsQueryBuilder.query(mBugsDbHelper.getReadableDatabase(),
                null,
                INSECT_BY_ID,
                new String[]{Integer.toString(id)},
                null,
                null,
                null);
    }

    /**
     * Insect db column names implementing {@link BaseColumns}
     */
    static final class InsectDbEntry implements BaseColumns {

        // Insect Db table name
        static final String TABLE_NAME = "insects";

        // Friendly name for the insect
        static final String COLUMN_FRIENDLY_NAME = "friendly_name";
        // Scientific name for the insect
        static final String COLUMN_SCIENTIFIC_NAME = "scientific_name";
        // Insect classification
        static final String COLUMN_CLASSIFICATION = "classification";
        // Insect image asset name
        static final String COLUMN_IMAGE_ASSET = "image_asset";
        // Insect's danger level
        static final String COLUMN_DANGER_LEVEL = "danger_level";
    }
}
