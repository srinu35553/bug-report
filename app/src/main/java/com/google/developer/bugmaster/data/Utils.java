package com.google.developer.bugmaster.data;

import android.support.v4.util.Pair;

import com.google.developer.bugmaster.data.Insect;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

/**
 * Utility class
 */
public class Utils {

    /**
     * Get a random list of insects with the specified count
     *
     * @param allInsects Total insects list
     * @param count Number of random insects to be returned
     * @return {@link Pair} containing list of random insects and the index in
     * that list representing the correct answer
     */
    public static Pair<ArrayList<Insect>, Integer> pickRandomInsects(List<Insect> allInsects, int count) {
        // Shuffle the items in the list to a random order
        List<Insect> randomInsects = new ArrayList<>(allInsects);
        Collections.shuffle(randomInsects);
        randomInsects = randomInsects.subList(0, count);

        // Generate random number within the range 0(inclusive) - count(exclusive)
        Random random = new Random();
        int answer = random.nextInt(count);
        return new Pair<>(new ArrayList<>(randomInsects), answer);
    }
}
