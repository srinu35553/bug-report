package com.google.developer.bugmaster;

import android.content.Intent;
import android.content.res.AssetManager;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.developer.bugmaster.data.Insect;

import java.io.IOException;
import java.io.InputStream;

public class InsectDetailsActivity extends AppCompatActivity {

    private static final String TAG = InsectDetailsActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_insect_detail);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        ImageView bugImage = (ImageView) findViewById(R.id.bug_image);
        TextView scientificName = (TextView) findViewById(R.id.scientific_name);
        TextView friendlyName = (TextView) findViewById(R.id.friendly_name);
        TextView classification = (TextView) findViewById(R.id.classification);
        RatingBar dangerLevelRating = (RatingBar) findViewById(R.id.danger_level_rating);

        Intent intent = getIntent();
        Insect insect = intent.getParcelableExtra(MainActivity.INSECT);

        // Get the insect details and bind to the views
        if(insect != null) {
            AssetManager assetManager = getAssets();
            try {
                InputStream inputStream = assetManager.open(insect.imageAsset);
                Drawable imageDrawable = Drawable.createFromStream(inputStream, null);
                bugImage.setImageDrawable(imageDrawable);
            } catch (IOException ex) {
                Log.e(TAG, "Error loading the insect's image from assets: " +
                        ex.getLocalizedMessage());
                ex.printStackTrace();
            }

            scientificName.setText(insect.scientificName);
            friendlyName.setText(insect.name);
            classification.setText(String.format(getString(R.string.classification),
                    insect.classification));
            dangerLevelRating.setRating(insect.dangerLevel);
        } else {
            // If there is an error, show a toast and finish the
            // details activity to go back to MainActivity
            Toast.makeText(this, "Error loading the details, try again!",
                    Toast.LENGTH_LONG).show();
            finish();
        }
    }
}
